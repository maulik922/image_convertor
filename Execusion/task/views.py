from django.shortcuts import render, redirect, HttpResponse
import pathlib
from PIL import Image
import os
import shutil
import zipfile
from io import StringIO, BytesIO
from django.views.generic import TemplateView
from rest_framework.response import Response
from wsgiref.util import FileWrapper
import pillow_heif
from pydrive.drive import GoogleDrive
from pydrive.auth import GoogleAuth

import zipfile

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import requests
from rest_framework.views import APIView



class LocalToGdriveView(APIView):
    def post(self, request):
        
        data = request.FILES.getlist('file')

        check_file = check_zip_or_normal(data)

        format = request.data['convert_to']

        __format = request.data['convert_from']

        folder = request.data['folder_name'] 
    
        if check_file is None:
            for image in data:
                file_name = str(image).split('.')[0]
                file_extension = str(image).split('.')[1]
                if file_extension != __format:
                    continue
                else:
                    if file_extension == 'heic':
                        pillow_heif.register_heif_opener()
                    img = Image.open(image)
                    image_convert(img, format, file_name, folder)

        else:
            zip_file(check_file[1], format, __format, folder)
            


        desktop = os.path.expanduser("~/Desktop/")


        path = os.path.join(desktop, f"{folder}")


        data = os.listdir(path)

        f = BytesIO()
        zip = zipfile.ZipFile(f, 'w')

        for d in data:
            zip.write(path+f'/{d}', d)
        zip.close() # Close

        

        desktop = os.path.expanduser("~/Desktop/")

        data = os.listdir(desktop+f'/{folder}')

        create_gdrive(data, folder)


      

        shutil.rmtree(path)

        
        return Response("Almost Done")

class LocalToLocalView(APIView):
    def post(self, request):
        data = request.FILES.getlist('file')

        check_file = check_zip_or_normal(data)

        format = request.data['convert_to']

        __format = request.data['convert_from']



        folder = "None"

        check_file = check_zip_or_normal(data)

        if check_file is None:
            for image in data:
                file_name = str(image).split('.')[0]
                file_extension = str(image).split('.')[1]
                if file_extension != __format:
                    continue
                else:
                    if file_extension == 'heic':
                        pillow_heif.register_heif_opener()
                    img = Image.open(image)
                    image_convert(img, format, file_name, folder)

        else:
            zip_file(check_file[1], format, __format, folder)
            


        desktop = os.path.expanduser("~/Desktop/")


        path = os.path.join(desktop, f"{folder}")


        data = os.listdir(path)

        f = BytesIO()
        zip = zipfile.ZipFile(f, 'w')

        for d in data:
            zip.write(path+f'/{d}', d)
        zip.close() # Close


        
        shutil.rmtree(path)

        response = HttpResponse(f.getvalue(), content_type="application/zip")
        response['Content-Disposition'] = 'attachment; filename=plant4bar.zip'
        response['file_name'] = "fileeee"
        return response

from pathlib import Path


class GdriveToLocalView(APIView):
    def post(self, request):

        format = request.data['convert_to']
        __format = request.data['convert_from']

        auth = GoogleAuth()
        auth.LocalWebserverAuth()

        drive = GoogleDrive(auth)

        drive_data = drive.ListFile({'q': f"'{request.data['link']}' in parents"}).GetList()

        # drive_data.GetContentFile(desktop,  mimetype="application/vnd.google-apps.folder")

       


        n = 1
        for i in drive_data:
            extension = pathlib.Path(str(i['title'])).suffix

            if extension.split('.')[1] != __format:
                return Response("nooooooooo")
                break
            else:    
                path = os.path.join(desktop, f"gimages")
                image = os.makedirs(path)
                path_data = os.path.join(desktop, f"gimages/sample{n}{extension}")
                i.GetContentFile(path_data)

        datas = os.listdir(path)

        folder = "gdrive"

        for data in datas:
            img = Image.open(path+'/'+data)
            image_convert(img, format, data.split('.')[0], folder)

        desktop_g = os.path.expanduser("~/Desktop/")


        path_g = os.path.join(desktop_g, f"{folder}")


        data = os.listdir(path_g)

        f = BytesIO()
        zip = zipfile.ZipFile(f, 'w')

        for d in data:
            zip.write(path_g+f'/{d}', d)
        zip.close() # Close


        shutil.rmtree(path_g)
        shutil.rmtree(path)

        response = HttpResponse(f.getvalue(), content_type="application/zip")
        response['Content-Disposition'] = 'attachment; filename=plant4bar.zip'
        response['file_name'] = "fileeee"
        return response    



def create_gdrive(data, folder):
    auth = GoogleAuth()
    auth.LocalWebserverAuth()

    drive = GoogleDrive(auth)

  
    folder_metadata = {'title' : folder, 'mimeType' : 'application/vnd.google-apps.folder'}
    folder_data = drive.CreateFile(folder_metadata)
    folder_data.Upload()

    for upload_file in data:
        path_image = desktop+str(folder)+'/'+str(upload_file)
        image_path = path_image

        img = Image.open(image_path)
        load = img.load()

        folderid = folder_data['id']
        file = drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": folderid}]})
        file.SetContentFile(load)
        file.Upload()


def image_convert(image, format,file_name, folder):


    desktop = os.path.expanduser("~/Desktop/")
    path = os.path.join(desktop, f"{folder}")

    if os.path.isdir(path) is False:
        os.mkdir(path)
    if format == 'heic':
        data_op = pillow_heif.from_pillow(
            image
        )
        data_op.save(path+f"/{file_name}.{format}")
    else:
        image.save(path+f"/{file_name}.{format}")



def zip_file(zip_data, format, only_format, folder):


    imgzip = zipfile.ZipFile(zip_data)
    inflist = imgzip.infolist()

    for f in inflist:
        extension = f.filename.split('.')[1]
        if extension != only_format:
            continue
        else:
            ifile = imgzip.open(f)
            file_name = f.filename.split('.')[0]
            if extension == 'heic':
                pillow_heif.register_heif_opener()
                img = Image.open(ifile)
            else:
                img = Image.open(ifile)
            image_convert(img, format, file_name, folder)

        


            

def check_zip_or_normal(data):
    if len(data) == 1:
            for d in data:
                print(d)
                file_data = pathlib.Path(str(d)).suffix
                if file_data == ".zip":
                    file_extension = "zip"
                    return file_extension, d
                else:
                    return None    
    
desktop = os.path.expanduser("~/Desktop/")

from pydrive.auth import GoogleAuth
GoogleAuth.DEFAULT_SETTINGS['client_config_file'] = f'{desktop}'+'image_convertor/Execusion/task/client_secrets.json'
import gdown

from google_drive_downloader import GoogleDriveDownloader as gdd




def ImageView(request):

   

    # for upload_file in data:
    #     gfile = drive.CreateFile({'parents': [{'id': '1pzschX3uMbxU0lB5WZ6IlEEeAUE8MZ-t'}]})
    #     # Read file and set it as the content of this instance.
    #     print(desktop+'images/'+upload_file)
    #     gfile.SetContentFile(desktop+'images/'+upload_file)
    #     gfile.Upload() # Upload the file.



    if request.method == 'POST'or  request.method == 'FILES':
        # desktop = os.path.expanduser("~/Desktop/")
        # path = os.path.join(desktop, f"lmao")


        gauth = GoogleAuth()           
        drive = GoogleDrive(gauth)  

        # data = "https://drive.google.com/drive/folders/1kc9rTu48eU4jQNxT2K54N2KZwqzQzvhk?usp=share_link"

        # drive_data = drive.ListFile({'q': "'1kc9rTu48eU4jQNxT2K54N2KZwqzQzvhk' in parents"}).GetList()
        
        # print(drive_data.GetContentFile(desktop,  mimetype="application/vnd.google-apps.folder"))

        # for i in drive_data:
            
        #     image = Image.open(i['id'])

        #     i.GetContentFile(path)

        # for data in drive_data:
            

        #     if data['id'] == "1kc9rTu48eU4jQNxT2K54N2KZwqzQzvhk":
        #         print(data,"======")

        #         # file2 = drive.CreateFile({'id': data['id']})
        #         data.GetContentFile(desktop, mimetype="application/vnd.google-apps.folder")


    
  

        # output = '20150428_collected_images.rar'
        # gdown.download(data, output, quiet=False)
        
        # gdd.download_file_from_google_drive(file_id='1iytA1n2z4go3uVCwE__vIKouTKyIDjEq',
        #                             dest_path='./data/mnist.zip',
        #                             unzip=True)

        data = request.FILES.getlist('data_shared')

        check_file = check_zip_or_normal(data)

        format = request.POST['format']

        __format = request.POST['takenfrom']

        folder = request.POST['folder']

        
    
        if check_file is None:
            for image in data:
                file_name = str(image).split('.')[0]
                file_extension = str(image).split('.')[1]
                if file_extension != __format:
                    continue
                else:
                    if file_extension == 'heic':
                        pillow_heif.register_heif_opener()
                    img = Image.open(image)
                    image_convert(img, format, file_name, folder)

        else:
            zip_file(check_file[1], format, __format, folder)
            


        desktop = os.path.expanduser("~/Desktop/")


        path = os.path.join(desktop, f"{folder}")


        data = os.listdir(path)

        f = BytesIO()
        zip = zipfile.ZipFile(f, 'w')

        for d in data:
            zip.write(path+f'/{d}', d)
        zip.close() # Close

        

        desktop = os.path.expanduser("~/Desktop/")

        data = os.listdir(desktop+f'/{folder}')


        folder_name = folder
        folder_metadata = {'title' : folder_name, 'mimeType' : 'application/vnd.google-apps.folder'}
        folder_data = drive.CreateFile(folder_metadata)
        folder_data.Upload()

        # Upload file to folder
    

        for umage in data:
            path_image = desktop+str(folder)+'/'+str(umage)
            image_path = path_image
            folderid = folder_data['id']
            file = drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": folderid}]})
            file.SetContentFile(image_path)
            file.Upload()
        

        shutil.rmtree(path)

        response = HttpResponse(f.getvalue(), content_type="application/zip")
        response['Content-Disposition'] = 'attachment; filename=plant4bar.zip'
        return response



# def gdrive_add(request):
#     auth = GoogleAuth()
#     auth.LocalWebserverAuth()

#     drive = GoogleDrive(auth)

    

        
   
  
        # Below code does the authentication
        # part of the code
        # gauth = GoogleAuth()
        
        # Creates local webserver and auto
        # handles authentication.
        # gauth.LocalWebserverAuth()       

        # print(gauth)
        # drive = GoogleDrive(gauth)

        # print(drive)
        
        # replace the value of this variable
        # with the absolute path of the directory
       
        
        # iterating thought all the files/folder
        # of the desired directory
        # for x in os.listdir(path):
        
        #     f = drive.CreateFile({'title': x})
        #     f.SetContentFile(os.path.join(path, x))
        #     f.Upload()     

        

        

        
    return render(request,'index.html')





class LocalToLocal(TemplateView):
    template_name = 'index.html'

class LocalToGdrive(TemplateView):
    template_name = 'index.html'    

class GdriveToGdrive(TemplateView):
    template_name = 'index.html'        


class GdriveToLocal(TemplateView):
    template_name = 'index.html'