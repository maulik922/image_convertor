from django.urls import path
from .views import *

urlpatterns = [
    path('', LocalToLocal.as_view(),name='localtolocal'),
    path('local_to_gdrive', LocalToGdrive.as_view(),name='localtogdrive'),
    path('gdrive_to_local', GdriveToLocal.as_view(),name='gdrivetolocal'),
    path('gdrive_to_gdrive', GdriveToGdrive.as_view(),name='gdrivetogdrive'),
    path('localtolocal_api/', LocalToLocalView.as_view(), name='localtolocalview'),
    path('localtogdrive_api/', LocalToGdriveView.as_view(), name='localtogdriveview'),
    path('gdrivetolocal_api/', GdriveToLocalView.as_view(), name='gdrivetolocalview'),

]